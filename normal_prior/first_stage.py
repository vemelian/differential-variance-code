import numpy as np
import scipy.stats as st
from scipy.optimize import newton,  bisect


class DecisionMaker(object):

    def __init__(self, mu, sigma, sigmag, pg, beta):
        self.mu = np.array(mu)
        self.sigma = np.array(sigma)
        self.sigmag = np.array(sigmag)
        self.beta = np.array(beta)
        self.pg = np.array(pg)
        self.sigmasum = np.sqrt(self.sigma**2 + self.sigmag**2) 

    def budget1(self, theta1):
        theta1 = np.array(theta1)
        return np.sum(self.pg * self.selection_proba1(theta1))

    def selection_proba1(self, theta1):
        theta1 = np.array(theta1)
        return 1 - st.norm(self.mu + self.beta, self.sigmasum).cdf(theta1)

    def utility1(self, theta1, a1):
        theta1 = np.array(theta1)
        return np.sum((self.sigma**2 * st.norm(0, self.sigmasum).pdf(self.mu + self.beta - theta1)\
         + self.mu * st.norm(0, self.sigmasum).cdf(self.mu + self.beta - theta1)) * self.pg) / a1

    def group_oblivious_threshold(self, a1):
        theta1 = newton(lambda x: self.budget1([x,x]) - a1, x0=0)
        return theta1 * np.ones(2)

    def gamma_threshold(self, a1, gamma=0.8):
        '''
        Gamma-OBL.
        '''
        theta_obl =  self.group_oblivious_threshold(a1)
        p1a, p1b = self.selection_proba1(theta_obl)
        pa, pb = self.pg
        if p1a / p1b <= gamma:
            p1b = a1 / (pb + gamma * pa)
            p1a = gamma * p1b
        elif p1b / p1a <= gamma:
            p1a = a1 / (pa + gamma * pb)
            p1b = gamma * p1a
        else:
            return theta_obl
        TOL=1e-10
        t1a = newton(lambda t1: self.selection_proba1([t1, 0])[0] - p1a, x0=1, tol=TOL)
        t1b = newton(lambda t1: self.selection_proba1([0, t1])[1] - p1b, x0=1, tol=TOL)
        return [t1a, t1b]

    def gamma_opt_threshold(self, a1, gamma=0.8, method='newton'):
        '''
        Gamma-OPT .
        '''
        theta_opt =  self.opt1_threshold(a1, method=method)
        p1a, p1b = self.selection_proba1(theta_opt)
        pa, pb = self.pg
        if p1a / p1b <= gamma:
            p1b = a1 / (pb + gamma * pa)
            p1a = gamma * p1b
        elif p1b / p1a <= gamma:
            p1a = a1 / (pa + gamma * pb)
            p1b = gamma * p1a
        else:
            return theta_opt
        TOL=1e-10
        t1a = newton(lambda t1: self.selection_proba1([t1, 0])[0] - p1a, x0=1, tol=TOL)
        t1b = newton(lambda t1: self.selection_proba1([0, t1])[1] - p1b, x0=1, tol=TOL)
        return [t1a, t1b]

    def dp_threshold(self, a1):
        quantile = st.norm().ppf(1 - a1)
        theta1 = self.sigmasum * quantile + self.mu + self.beta
        return theta1

    def opt1_threshold(self, a1, method='newton'):
        C = self.group_oblivious_threshold(a1)[0]
        Cinit = (C - self.mu - self.beta) * self.sigma**2 / self.sigmasum** 2 + self.mu
        if method == 'newton':
            C = newton(lambda C: self.budget1((np.array([C, C])  - self.mu)* self.sigmasum**2 / self.sigma**2 + self.mu + self.beta) - a1,\
            x0=Cinit[0]+1e-10, x1=Cinit[1])
        elif method == 'bisect':
            C = bisect(lambda C: self.budget1((np.array([C, C])  - self.mu)* self.sigmasum**2 / self.sigma**2 + self.mu + self.beta) - a1,\
             a=Cinit[0]-1e-10, b=Cinit[1])
        theta1 = (C  - self.mu)* self.sigmasum**2 / self.sigma**2 + self.mu + self.beta
        return theta1
