from .util import get_top
import numpy as np


def dp(x, q, g, a1):
    """
    DP policy.
    """
    n = x.shape[0]
    n1 = np.ceil(n * a1).astype(int)
    nA = np.sum(g)
    I = np.array(range(len(x)))
    
    # calculate selection sizes
    n1A = np.ceil(nA * a1).astype(int)
    n1B = n1 - n1A

    # perform selection
    I1A = get_top(x[g==1], n1A)
    I1B = get_top(x[g==0], n1B)
    I1 = np.concatenate((I[g==1][I1A], I[g==0][I1B]))
    return I1
