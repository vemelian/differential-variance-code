from .util import get_top
import numpy as np
from .greedy import greedy


def beta(x, q, g, a1, beta=0.8):
    """
    Gamma rule.
    """
    n = x.shape[0]
    n1 = np.ceil(n * a1).astype(int)

    nA = g.sum()
    nB= n - nA
    # perform greedy
    I1, _ = greedy(x, q, g, a1)
    n1A = g[I1].sum()
    n1B = n1 - n1A

    # check gamma-condition
    if (n1A / nA) / (n1B / nB) <= beta:
        n1B = np.ceil(n1 / (1 + beta * nA / nB)).astype(int)
        n1A = n1 - n1B
    elif (n1B / nB) / (n1A / nA)  <= beta:
        n1A = np.ceil(n1 / (1 + beta * nB / nA)).astype(int)
        n1B = n1  - n1A

    # perform new round of selection
    I = np.array(range(len(x)))
    I1A = get_top(x[g==1], n1A)
    I1B = get_top(x[g==0], n1B)
    I1 = np.concatenate((I[g==1][I1A], I[g==0][I1B]))
    return I1
