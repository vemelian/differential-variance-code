from .util import get_top
import numpy as np


def greedy(x, q, g, a1):
    """
    Greedy Policy.
    """
    n = x.shape[0]
    n1 = np.ceil(n * a1).astype(int)
    I1 = get_top(x, n1)
    return I1
